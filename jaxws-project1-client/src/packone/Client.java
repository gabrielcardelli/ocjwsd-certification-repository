package packone;

public class Client {

	public static void main( String[] args ) {
		HelloService service = new HelloService();
		Hello hello = service.getHelloPort();
		System.out.println( hello.sayHello( "Gabriel" ) );
	}

}
