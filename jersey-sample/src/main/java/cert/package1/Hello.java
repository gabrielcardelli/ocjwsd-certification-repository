package cert.package1;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path( "/hello" )
public class Hello {

	@Context
	ServletContext context;

	@GET
	@Path( "/text" )
	@Produces( MediaType.TEXT_PLAIN )
	public String sayPlainTextHello() {

		return "Hello Jersey in plain text";

	}

	@GET
	@Path( "/text/{nome}" )
	@Produces( MediaType.TEXT_PLAIN )
	public String sayPlainTextHelloWithParams( @PathParam( value = "nome" ) String nome ) {

		return "Hello " + nome + " in plain text";

	}

	@GET
	@Path( "/html/{nome}" )
	@Produces( MediaType.TEXT_HTML )
	public String sayHtmlHello( @PathParam( value = "nome" ) String nome ) {
		return "<html> " + "<title>" + "Hello Jersey" + "</title>" + "<body><h1>" + "Hello " + nome + " in HTML" + "</body></h1>" + "</html>";
	}

	@GET
	@Path( "/xml" )
	@Produces( MediaType.TEXT_XML )
	public String sayXMLHello() {
		return "<?xml version=\"1.0\"?>" + "<hello> Hello Jersey in XML </hello>";
	}

}
