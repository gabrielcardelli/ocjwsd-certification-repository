package cert.package1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public HelloServlet() {
		super();
	}

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		Client client = Client.create();
		WebResource webResource = client.resource( "http://localhost:8080/jersey-sample/rest/hello/text" );
		String result = webResource.get( String.class );

		response.getWriter().println( "== Consumindo WS Text Plain == \n" );
		response.getWriter().println( result );
		response.getWriter().println( "\n\n" );

		webResource = client.resource( "http://localhost:8080/jersey-sample/rest/hello/text/gabriel" );
		result = webResource.get( String.class );

		response.getWriter().println( "== Consumindo WS enviando params == \n" );
		response.getWriter().println( result );

	}

	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		doGet( request, response );
	}

}
