import javax.xml.ws.Endpoint;

import packOne.Hello;

public class WSPublisher {

	public static void main( String[] args ) {
		Endpoint.publish( "http://localhost:8080/WS/Hello", new Hello() );
	}

}
